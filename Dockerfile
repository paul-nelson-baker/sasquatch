FROM node:8

#ADD wait-for-it.sh ./
#RUN chmod +x ./wait-for-it.sh

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
#COPY ./wait-for-it.sh ./wait-for-it.sh

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 8080

#ENTRYPOINT ["npm", "start", "--host", "0.0.0.0"]
CMD ["npm", "start"]