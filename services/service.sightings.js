'use strict';
module.exports = (connection) => {

    let sightingsService = {};

    /**
     * @method: _sanitizeString @private
     * @param: {string} str
     * @notes: lets strip any ' or " from the url params to prevent sql injection
     */
    function _sanitizeString(str) {
        return str.replace(/["']/g, "");
    }

    function _queryDatabase(querySting) {
        return new Promise( (resolve, reject) => {
            connection.query(querySting, (err, rows, fields) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(rows);
                }
            });
        });
    }

    /**
     * @method: getSightings
     * @param: none
     * @notes:
     * @endpoint: /sightings/
     */
    sightingsService.getSightings = () => {
        let query = `SELECT id, latitude, longitude, time, description, tags FROM sightings WHERE active = '1'`;
        return _queryDatabase(query);
    };

    /**
     * @method: getSightingsById
     * @param: {string}
     * @notes:
     * @endpoint: /sightings/{sightingId}
     */
    sightingsService.getSightingsById = (id) => {
        id = _sanitizeString(id);
        let query = `SELECT id, latitude, longitude, time, description, tags FROM sightings WHERE id = ${id}`;
        return _queryDatabase(query);
    };

    /**
     * @method: getSightingsBytagName
     * @param: {strings}
     * @notes:
     * @endpoint: /sightings/tags/{anyTagNameYouWant}
     */
    sightingsService.getSightingsBytagName = (tagName) => {
        tagName = _sanitizeString(tagName);
        let query = `SELECT id, latitude, longitude, time, description, tags FROM sightings WHERE tags LIKE '%${tagName}%';`;
        return _queryDatabase(query);
    };

    /**
     * @method: getCordsFromTwoIds
     * @param: {strings}
     * @notes: get the coordinates from the db of the 2 ids given
     * @endpoint: /sightings/distance/record-one/{sightingId}/record-two/{sightingId}
     */
    sightingsService.getCordsFromTwoIds = (id1, id2) => {
        let ids = [_sanitizeString(id1), _sanitizeString(id2)].join(','),
            query = `SELECT latitude, longitude FROM sightings WHERE FIND_IN_SET(id, '${ids}') LIMIT 2`;
        return _queryDatabase(query);
    };

    /**
     * @method: updateSighting
     * @param: {object} payload.latitude, payload.longitude, payload.descripton, payload.time, payload.descripton, payload.tags
     * @notes: using connection.escape() for every input value to prevent sql injection
     * @endpoint: /sightings/create
     */
    sightingsService.createSighting = (payload) => {
        let latitude    = connection.escape(payload.latitude),
            longitude   = connection.escape(payload.longitude),
            time        = connection.escape(payload.time),
            description = connection.escape(payload.description),
            tags        = connection.escape(payload.tags);

        let query = `INSERT INTO sightings (id, latitude, longitude, time, description, tags)
                     VALUES (NULL, ${latitude}, ${longitude}, ${time}, ${description}, ${tags})`;

        return _queryDatabase(query);
    };

    /**
     * @method: updateSighting
     * @param: {object} payload.id, payload.latitude, payload.longitude, payload.descripton
     * @notes:
     * @endpoint: /sightings/update/{sightingId}
     */
    sightingsService.updateSighting = (payload) => {
        let query,
            id = _sanitizeString(payload.id),
            latitude    = connection.escape(payload.latitude),
            longitude   = connection.escape(payload.longitude),
            description = connection.escape(payload.description);

        if (payload.latitude && payload.longitude) {
            query = `UPDATE sightings SET latitude = ${latitude}, longitude = ${longitude} WHERE id = ${id}`;
        } else if (payload.description) {
            query = `UPDATE sightings SET description = ${description} WHERE id = ${id}`;
        }

        return _queryDatabase(query);
    };

    /**
     * @method: updateTags
     * @param: {object} payload.id, payload.tags
     * @notes: I know this is not the kosher way of doing this, however due to time constraints, it works.
     * @endpoint: /sightings/update/tags/{sightingId}
     */
    sightingsService.updateTags = (payload) => {
        let id     = _sanitizeString(payload.id),
            q = `SELECT CASE WHEN tags IS NULL OR tags = '' THEN 'empty' ELSE tags END AS tags FROM sightings WHERE id = ${id}`;

        return _queryDatabase(q).then( data => {
            if (data[0].tags === 'empty')  {
                let newTag = connection.escape(payload.tags),
                    query = `UPDATE sightings SET tags = ${newTag} WHERE id = ${id}`;
                return _queryDatabase(query);
            } else {
                let newTag = connection.escape(','+ payload.tags),
                    query = `UPDATE sightings SET tags = CONCAT(tags, ${newTag}) WHERE id = ${id}`;
                return _queryDatabase(query);
            }
        });
    };

    /**
     * @method: deleteTagById
     * @param: {object} payload.id, payload.tag
     * @notes: I know this won't work in most cases, I didn't have time to properly execute tag deletion
     * @endpoint: /sightings/update/tags/{sightingId}
     */
    sightingsService.deleteTagById = (payload) => {
        let id = _sanitizeString(payload.id),
            tagToDelete = connection.escape(','+ payload.tags),
            query = `UPDATE sightings SET tags = REPLACE(tags, ${tagToDelete}, '') WHERE id = ${id}`;
        return _queryDatabase(query);
    };

    /**
     * @method: deleteSightingsById
     * @param: {string} id
     * @notes: Do NOT actually DELETE row in table just mark it as `inactive`
     * @endpoint: /sightings/delete/{sightingId}
     */
    sightingsService.deleteSightingsById = (id) => {
        id = _sanitizeString(id);
        let query = `UPDATE sightings SET active = '0' WHERE id = ${id}`;
        return _queryDatabase(query);
    };

    return sightingsService;
};