/**************************************************************
 *  @Author: John Kay
 *  @Date: December 12, 2018
 *  @Project: Sasquatch Sightings API
 *  @Notes:
 *          Docker commands:
 *              docker build --tag sasquatch .
 *              docker-compose up -d
 *              docker-compose down
 *                            ____
 *                         _.' :  `._
 *                     .-.'`.  ;   .'`.-.
 *            __      / : ___\ ;  /___ ; \      __
 *          ,'_ ""--.:__;".-.";: :".-.":__;.--"" _`,
 *          :' `.t""--.. '<@.`;_  ',@>` ..--""j.' `;
 *               `:-.._J '-.-'L__ `-- ' L_..-;'
 *                 "-.__ ;  .-"  "-.  : __.-"
 *                     L ' /.------.\ ' J
 *                      "-.   "--"   .-"
 *                     __.l"-:_JL_;-";.__
 *                  .-j/'.;  ;""""  / .'\"-.
 *                .' /:`. "-.:     .-" .';  `.
 *             .-"  / ;  "-. "-..-" .-"  :    "-.
 *          .+"-.  : :      "-.__.-"      ;-._   \
 *          ; \  `.; ;                    : : "+. ;
 *          :  ;   ; ;                    : ;  : \:
 *         : `."-; ;  ;                  :  ;   ,/;
 *         ;    -: ;  :                ;  : .-"'  :
 *          :\     \  : ;             : \.-"      :
 *           ;`.    \  ; :            ;.'_..--  / ;
 *           :  "-.  "-:  ;          :/."      .'  :
 *             \       .-`.\        /t-""  ":-+.   :
 *              `.  .-"    `l    __/ /`. :  ; ; \  ;
 *                \   .-" .-"-.-"  .' .'j \  /   ;/
 *                 \ / .-"   /.     .'.' ;_:'    ;
 *                  :-""-.`./-.'     /    `.___.'
 *                        \ `t  ._  /  bug :F_P:
 *                         "-.t-._:'
 **************************************************************/
'use strict';
let PORT = 8080;
let express = require('express');
let server  = express();
let connection = require('./db/connect');
let controllers = require('./controllers/index')(connection);
server.use('/', controllers.sightingsController);
server.use('*', (req, res) => {
    res.send('Endpoint NOT found');
});

server.listen(PORT, () => {
    console.log(`Running on port: ${PORT}`);
});
