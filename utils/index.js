/**
 * Designed this way to scale, just add your module here
 */

let object = {};
object.calculateDistance = require('./calculate-distance');
module.exports = object;