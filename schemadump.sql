# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Generation Time: 2018-12-12 06:26:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table sightings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sightings`;

CREATE TABLE `sightings` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `latitude` varchar(225) NOT NULL,
                           `longitude` varchar(225) NOT NULL,
                           `time` varchar(225) NOT NULL,
                           `description` varchar(255) NOT NULL,
                           `tags` varchar(225) NOT NULL DEFAULT '',
                           `active` tinyint(4) NOT NULL DEFAULT '1',
                           PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `sightings` WRITE;
/*!40000 ALTER TABLE `sightings` DISABLE KEYS */;

INSERT INTO `sightings` (`id`, `latitude`, `longitude`, `time`, `description`, `tags`, `active`)
VALUES
(1,'40.413590','-111.876959','12:00pm','I know this sounds crazy but I think I just saw Big Foot!','I am not crazy,lake',1),
(2,'40.750915','-111.918591','11:00am','He is a lot harrier in person.','snow,caves',1),
(3,'43.663215','-116.177379','5:00pm','I just saw sasquatch in Boise!','snow,mountains',1),
(4,'38.580178','-104.826686','5:30pm','Yup he\'s here in Colorado Springs.','caves,mountains',1),
	(5,'42.020059','-111.400733','11:00am','I could have sworn I saw big foot on a paddle boat on Bear Lake.','lake,paddle boat,water',1),
	(6,'43.490261','-112.069006','1:00am','Sasquatch must have traveled along way to get to Idaho Falls.','Idaho,lake,long distance,caves',1);

/*!40000 ALTER TABLE `sightings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
