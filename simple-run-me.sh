#!/usr/bin/env bash
set -e
./build-docker-image.sh
docker-compose down
docker-compose up
