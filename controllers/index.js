/**
 * Designed this way to scale, just add your module here
 * pass the db connection to controller
 */

module.exports = (connection) => {
    let object = {};
    object.sightingsController = require('./controller.sightings')(connection);
    return object;
};