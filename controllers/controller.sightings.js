'use strict';
let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let path = require('path');
let getDistanceBetweenCoordinates = require('../utils/index');
// use body parser so we can get info from POST and/or URL parameters
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

module.exports = (connection) => {
    let sightings = require('../services/service.sightings')(connection);

    function _errorObject() {
        return {
            message: "Something went wrong",
            success: false,
            status: 500
        }
    }

    function _successObject() {
        return {
            success: true,
            status: 200
        }
    }

    router.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, '../index.html'));
    });

    router.get('/sightings', (req, res) => {
        let responseJson = {};
        sightings.getSightings().then((data) => {
            responseJson["sightings"] = data;
            res.json(responseJson);
        });
    });

    router.get('/sightings/:id', (req, res) => {
        let responseJson = {},
            id = req.params.id;

        sightings.getSightingsById(id).then((data) => {
            responseJson["sightings"] = data;
            res.json(responseJson);
        });
    });

    router.get('/sightings/tags/:tagName', (req, res) => {
        let responseJson = {},
            tagName= req.params.tagName;

        sightings.getSightingsBytagName(tagName).then((data) => {
            responseJson["sightings"] = data;
            res.json(responseJson);
        });
    });

    router.get('/sightings/distance/record-one/:recordOneId/record-two/:recordTwoId', (req, res) => {
        let responseJson = {},
            id1 = req.params.recordOneId,
            id2 = req.params.recordTwoId;

        sightings.getCordsFromTwoIds(id1, id2).then((data) => {
            let theDistanceBetweenTwoIds = getDistanceBetweenCoordinates.calculateDistance(data[0].latitude, data[0].longitude, data[1].latitude, data[1].longitude);
            responseJson["sightings"] = data;
            responseJson["distance"]  = theDistanceBetweenTwoIds;
            res.json(responseJson);
        });
    });

    router.post('/sightings/create', (req, res) => {
        let payload = req.body;

        if (payload.latitude && payload.longitude && payload.time && payload.description) {
            sightings.createSighting(payload).then((data) => {
                let responseObject;

                if (data.affectedRows === 1) {
                    responseObject = _successObject();
                    responseObject['message'] = "Created Successfully";
                } else {
                    responseObject = _errorObject();
                }

                res.json(responseObject);
            });
        } else {
            res.json(_errorObject());
        }
    });

    router.put('/sightings/update/:sightingId', (req, res) => {
        let payload = req.body;
        payload.id  = req.params.sightingId;

        if (payload.latitude && payload.longitude || payload.description) {
            sightings.updateSighting(payload).then((data) => {
                if (data.affectedRows === 1) {
                    let responseObject;

                    if (data.affectedRows === 1) {
                        responseObject = _successObject();
                        responseObject['message'] = "Updated Successfully";
                    } else {
                        responseObject = _errorObject();
                    }

                    res.json(responseObject);
                }
            });
        } else {
            res.json(_errorObject());
        }
    });

    router.put('/sightings/update/tags/:sightingId', (req, res) => {
        let payload = req.body;
        payload.id  = req.params.sightingId;

        if (payload.tags) {
            sightings.updateTags(payload).then((data) => {
                if (data.affectedRows === 1) {
                    let responseObject;

                    if (data.affectedRows === 1) {
                        responseObject = _successObject();
                        responseObject['message'] = "Updated Successfully";
                    } else {
                        responseObject = _errorObject()
                    }

                    res.json(responseObject);
                }
            });
        } else {
            res.json(_errorObject());
        }
    });

    router.put('/sightings/delete/:sightingId', (req, res) => {
        let id = req.params.sightingId;

        if (id) {
            sightings.deleteSightingsById(id).then((data) => {
                if (data.affectedRows === 1) {
                    let responseObject;

                    if (data.affectedRows === 1) {
                        responseObject = _successObject();
                        responseObject['message'] = "Updated Successfully";
                    } else {
                        responseObject = _errorObject();
                    }

                    res.json(responseObject);
                }
            });
        } else {
            res.json(_errorObject());
        }
    });

    router.put('/sightings/delete/tags/:sightingId', (req, res) => {
        let payload = req.body;
        payload.id  = req.params.sightingId;

        sightings.deleteTagById(payload).then((data) => {
            if (data.affectedRows === 1) {
                let responseObject;

                if (data.affectedRows === 1) {
                    responseObject = _successObject();
                    responseObject['message'] = "Tag Removed Successfully";
                } else {
                    responseObject = _errorObject();
                }

                res.json(responseObject);
            }
        });
    });

    return router;
};