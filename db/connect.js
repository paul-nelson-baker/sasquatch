'use strict';
let mysql = require('mysql');
let connection = mysql.createConnection({
    host     : 'mysqldb',
    user     : 'test',
    password : 'test',
    database : 'test',
    port     : 3306
});

connection.connect((err) => {
    if(err){
        console.log('Error connecting to Db', err);
        return;
    }
    console.log('Connection established');
});

module.exports = connection;